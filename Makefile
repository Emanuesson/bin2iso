OBJS = bin2iso.o
BIN = bin2iso
CFLAGS = -O2 -Wall -std=gnu99

ifeq ($(OS),Windows_NT)
	LDFLAGS =
	CFLAGS += -s -static
else
	LDFLAGS =
endif

all: $(BIN)

$(BIN): $(OBJS)
	$(CC) $(CFLAGS) -o $(BIN) $(OBJS) $(LDFLAGS)

clean:
	$(RM) ${OBJS} ${BIN}
