bin2iso
-------

A commandline tool for converting `bin` + `cue` files into `iso` image files and
separate `wav` audio tracks. This was originally by Bob Doiron, but he moved on
after version 1.9b. Therefore I am adding some further maintenance fixes in this repo.

Original source locations, possibly inaccessible now:
- http://xpt.sourceforge.net/techdocs/media/video/dvdvcd/dv09-NonIsoCDFormats/ar01s10.html
- http://users.eastlink.ca/~doiron/bin2iso/linux/
- http://users.andara.com/~doiron
- http://slackware.uk/sbosrcarch/by-name/system/bin2iso/

### Usage

Convert a `cue` pointing to a valid `bin`:
```
bin2iso <cuefile>
```

Try to generate a `cue` file from a cueless `bin`:
```
bin2iso <cuefile> -c <binfile>
```

All parameters:
```
bin2iso <cuefile> [<output dir>] [-naib2] [-t X]

Where:
<cuefile>     The .cue file that belongs to the .bin file being converted.
<output dir>  The output directory (defaults to current dir).
-n --nogaps   Indicates that audio data found in the track 'gaps' should
              not be appended to the wave files.
-a --gaps     Looks for non-zero data in the 'gaps'; if found, then gaps
              are appended to wave files. Looks for more than 1/2 of
              a sector of non-zero values (588 values).
-t --track=X  Extracts only the X'th track.
-i --inplace  Performs the conversion 'in place'; ie. truncates the binfile
              after each track is created to minimize space usage.
              (can't be used with -t)
-b --nob      Do not use overburn data past 334873 sectors.
              This of course presumes that the data is not useful.
-c --cuefrom=<binfile>
              Attempts to create a <cuefile> from an existing <binfile>.
-2 --mode2to1 Attempts to convert mode2 ISO data to mode1.
```

### Building

Run `make`.

Oh, you're on Windows? I'm sorry. Set up msys32 with mingw32, copy `bin2iso.c` and
`Makefile` into your mingw32 home directory, which will look something like
`C:\msys32\home\<username>\`. Run mingw32. In the ming32 window, run `make`.

Or you can use the prebuilt exe:
- [bin2iso.exe](https://mooncore.eu/filus/bin2iso.exe) (1.9d, 37376 bytes, sha256 4b341a8c7921f645c1c3f1851be2daa10f9acb103a4f8f93987f72fa84b2fdca)
- [bin2iso19c.exe](https://mooncore.eu/filus/bin2iso19c.exe) (1.9c, 34304 bytes, sha256 d5e1340b814c381bca79922e3c2261cf5e46a1af9a24ef0c0dd290fe78ca4191)

### License

The original author did not specify an explicit license, but it seems this tool was
released in a spirit of free sharing, so can be considered freeware, or an implied WTFPL.
