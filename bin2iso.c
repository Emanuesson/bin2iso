#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <unistd.h>
#include <dirent.h>
#include <ctype.h>
#include "getopt.h"

#define DEBUG 0
#define CHECK 0 /* don't bother checking bin for validity... */
#define VERSIONSTR "V1.9d"

//----------------Wave Stuff---------------------/
typedef struct wavHdr
{
	char riff[4];
	uint32_t bytesToEnd;
	char waveTxt[4];
	char fmtTxt[4];
	uint32_t formatSize;  // 16 byte format specifier
	uint16_t format;      // Windows PCM
	uint16_t channels;    // 2 channels
	uint32_t sampleRate;  // 44,100 Samples/sec
	uint32_t avgByteRate; // 176,400 Bytes/sec
	uint16_t sampleBytes; // 4 bytes/sample
	uint16_t channelBits; // 16 bits/channel
	char dataTxt[4];
	uint32_t blockSize;
} tWavHead;

#define HEADBYTES 36
#define WINDOWS_PCM 0x0001
//-------------------------------------------------/

/* GLOBAL DEFINES */
#define SIZERAW 2352
#define SIZEISO_MODE1 2048
#define SIZEISO_MODE2_RAW 2352
#define SIZEISO_MODE2_FORM1 2048
#define SIZEISO_MODE2_FORM2 2336
#define AUDIO 0
#define MODE1 1
#define MODE2 2
#define MODE1_2352 10
#define MODE2_2352 20
#define MODE1_2048 30
#define MODE2_2336 40

#define RAWDATA FF // using this for leftover data when truncating for non-overburn

#define PROG_INTERVAL 1024
#define UNKNOWN -1
#define OFFSET 150
// got this from easycd pro by looking at a blank disk so it may be off...
#define CD74_MAX_SECTORS 334873 // 653.75 Mb

struct CommandlineOptions
{
	int8_t writegap;
	int32_t oneTrackNum;
	bool no_overburn, createCue, doOneTrack, doInPlace, mode2to1;
	char outputDir[256];
	char sBinFilename[256];
	char sCueFilename[256];
	char sOutFilename[256];
} options;

// global variables
FILE* fdBinFile;
FILE* fdCueFile;
FILE* fdOutFile;

#define OUTBUF_SIZE 4 * 1024 * 1024
#define INBUF_SIZE 4 * 1024 * 1024
uint8_t outBuf[OUTBUF_SIZE];
uint32_t outBufIndex = 0;
uint8_t inBuf[INBUF_SIZE];
uint32_t inBufReadIndex = 0;
uint32_t inBufWriteIndex = 0;

off_t writePos = 0; // for inplace conversions...

typedef struct track
{
	uint16_t mode;
	off_t idx0;
	off_t idx1;
	int32_t num;
	char name[80];
	off_t offset0;
	off_t offset1;
	size_t size; // track size in bytes
} tTrack;

void strcpylower(char* out_str, char* in_str)
{
	while (*in_str != 0)
		*out_str++ = tolower(*(in_str++));
	*out_str = 0;
}

uint32_t Index(uint8_t m, uint8_t s, uint8_t f)
{
	uint32_t temp;

	temp = (((m >> 4) * 10) + (m & 0xF)) * 60;
	temp = (temp + (((s >> 4) * 10) + (s & 0xF))) * 75;
	temp = temp + (((f >> 4) * 10) + (f & 0xF));

	//printf("\n%d%d %d%d %d%d = %06d", m >> 4, m & f, s >> 4, s & f, f >> 4, f & f, temp);
	return temp;
}

void UnIndex(uint32_t index, char* ptr)
{
	uint8_t m, s, f;

	f = (uint8_t) (index % 75);
	s = (uint8_t) ((index / 75) % 60);
	m = (uint8_t) (index / (75 * 60));
	sprintf(ptr, "%d%d:%d%d:%d%d", m / 10, m % 10, s / 10, s % 10, f / 10, f % 10);
}

bool BufferedFRead(uint8_t* array, size_t size)
{
	if (inBufWriteIndex == 0)
		inBufWriteIndex += fread(inBuf, 1, (INBUF_SIZE / size) * size, fdBinFile);

	if (inBufWriteIndex == 0)
		return false; // read failed

	for (size_t i = 0; i < size; i++)
	{
		array[i] = inBuf[inBufReadIndex++];
		if ((inBufReadIndex == inBufWriteIndex) && (i < (size - 1)))
		{
			printf("   Warning: Premature EOF\n");
			while (i++ < size) // zero fill the rest
			{
				array[i] = 0;
			}
			break;
		}
	}

	if (inBufReadIndex == inBufWriteIndex)
	{
		inBufReadIndex = 0;
		inBufWriteIndex = 0;
	}

	return true; // read passed
}

void BufferedFWrite(uint8_t* array, size_t size)
{
	if (outBufIndex + size >= OUTBUF_SIZE)
	{
		off_t readpos = 0;
		if (fdOutFile == fdBinFile)
		{
			readpos = ftell(fdOutFile);
			if (0 != fseek(fdOutFile, writePos, SEEK_SET))
			{
				perror("\nbin2iso(fseek)");
				exit(1);
			}
		}

		//printf("\nWriting\n");
		if (1 != fwrite(outBuf, outBufIndex, 1, fdOutFile))
		{
			perror("\nbin2iso(fwrite)");
			fclose(fdOutFile);
			// remove(options.sOutFilename);
			exit(1);
		}
		if (1 != fwrite(array, size, 1, fdOutFile))
		{
			perror("\nbin2iso(fwrite)");
			fclose(fdOutFile);
			// remove(options.sOutFilename);
			exit(1);
		}
		//printf("\nWrote %d bytes\n", outBufIndex + size);
		outBufIndex = 0;

		if (fdOutFile == fdBinFile)
		{
			writePos = ftell(fdOutFile);
			if (0 != fseek(fdOutFile, readpos, SEEK_SET))
			{
				perror("\nbin2iso(fseek)");
				exit(1);
			}
		}
	}
	else
	{
		for (size_t idx = 0; idx < size; idx++)
		{
			outBuf[outBufIndex + idx] = array[idx];
		}
		outBufIndex += size;
	}
}

void FlushBuffers(void)
{
	off_t readpos = 0;

	if (fdOutFile == fdBinFile)
	{
		readpos = ftell(fdOutFile);
		if (0 != fseek(fdOutFile, writePos, SEEK_SET))
		{
			perror("\nbin2iso(fseek)");
			exit(1);
		}
	}

	if (1 != fwrite(outBuf, outBufIndex, 1, fdOutFile))
	{
		perror("\nbin2iso(fwrite)");
		fclose(fdOutFile);
		// remove(options.sOutFilename);
		exit(1);
	}

	//printf("\nWrote %d bytes\n", outBufIndex);
	outBufIndex = 0;
	inBufReadIndex = 0;
	inBufWriteIndex = 0;

	if (fdOutFile == fdBinFile)
	{
		writePos = ftell(fdOutFile);
		if (0 != fseek(fdOutFile, readpos, SEEK_SET))
		{
			perror("\nbin2iso(fseek)");
			exit(1);
		}
	}
}

// presumes Line is preloaded with the "current" line of the file
bool GetTrackInfo(char* line, tTrack* track)
{
	track->idx0 = -1;
	track->idx1 = -1;

	// Get the 'mode'
	if (strncmp(&line[2], "TRACK ", 6) != 0)
		return false;

	char numstr[3];
	char* strend;
	numstr[0] = line[8];
	numstr[1] = line[9];
	numstr[2] = 0;
	track->num = strtol(numstr, &strend, 10);

	track->mode = UNKNOWN;
	if (strncmp(&line[11], "AUDIO", 5) == 0) track->mode = AUDIO;
	if (strncmp(&line[11], "MODE1/2352", 10) == 0) track->mode = MODE1_2352;
	if (strncmp(&line[11], "MODE1/2048", 10) == 0) track->mode = MODE1_2048;
	if (strncmp(&line[11], "MODE2/2352", 10) == 0) track->mode = MODE2_2352;
	if (strncmp(&line[11], "MODE2/2336", 10) == 0) track->mode = MODE2_2336;

	// Set the name
	strcpy(track->name, options.sBinFilename);
	track->name[strlen(options.sBinFilename) - 4] = '\0';
	strcat(track->name, "-");
	strcat(track->name, numstr);

	if ((track->mode == MODE1_2352) ||
		(track->mode == MODE1_2048) ||
		(track->mode == MODE2_2352) ||
		(track->mode == MODE2_2336))
	{
		strcat(track->name, ".iso");
	}
	else if (track->mode == AUDIO)
	{
		strcat(track->name, ".wav");
	}
	else
	{
		printf("Track %s Unsupported mode\n", numstr);
		return false;
	}

	// Get the track indexes
	while (1)
	{
		if (!fgets(line, 256, fdCueFile))
			break;

		if (strncmp(&line[2], "TRACK ", 6) == 0)
			break; // next track starting

		if (strncmp(&line[4], "INDEX ", 6) == 0)
		{
			char inum[3];
			strncpy(inum, &line[10], 2); inum[2] = '\0';
			char min = ((line[13] - '0') << 4) | (line[14] - '0');
			char sec = ((line[16] - '0') << 4) | (line[17] - '0');
			char block = ((line[19] - '0') << 4) | (line[20] - '0');

			if (strcmp(inum, "00") == 0)
				track->idx0 = Index(min, sec, block);
			else if (strcmp(inum, "01") == 0)
				track->idx1 = Index(min, sec, block);
			else
			{
				printf("Unexpected Index number: %s\n", inum);
				exit(1);
			}
		}
		else if (strncmp(&line[4], "PREGAP ", 7) == 0) { ; /* ignore, handled below */ }
		else if (strncmp(&line[4], "FLAGS ", 6) == 0) { ; /* ignore */ }
		else
		{
			printf("Unexpected cuefile line: %s\n", line);
		}
	}
	if (track->idx0 == -1)
		track->idx0 = track->idx1;
	if (track->idx1 == -1)
		track->idx1 = track->idx0;
	return true;
}

void DoTrack(int16_t mode, int32_t preidx, int32_t startidx, int32_t endidx, off_t offset)
{
	uint8_t buf[SIZERAW + 100];
	uint32_t blockswritten = 0;
	uint32_t uiLastIndex;
	#if CHECK
	uint32_t uiCurrentIndex;
	#endif
	uint32_t write = 1;

	tWavHead wavhead =
	{
		"RIFF",
		0,
		"WAVE",
		"fmt ",
		16,          // 16 byte format specifier
		WINDOWS_PCM, // format
		2,           // 2 Channels
		44100,       // 44,100 Samples/sec
		176400,      // 176,400 Bytes/sec
		4,           // 4 bytes/sample
		16,          // 16 bits/channel
		"data",
		0 };

	uiLastIndex = startidx - 1;
	// Input -- process -- Output
	if (startidx != 0)
		printf("\nNote: PreGap = %d frames\n", startidx - preidx);
	else
		printf("\nNote: PreGap = %d frames\n", OFFSET); // cd standard: starting offset
	// - of course this isn't true for bootable cd's...

	if (options.sOutFilename[0] != '\0')
		printf("Creating %s (%06d,%06d) ", options.sOutFilename, startidx, endidx - 1);
	else
		printf("Converting (%06d,%06d) ", startidx, endidx - 1);

	switch (mode)
	{
		case AUDIO:
			printf("Audio");
			break;
		case MODE1_2352:
			printf("Mode1/2048");
			break;
		case MODE2_2336:
			printf("Mode2/2352");
			break;
		case MODE2_2352:
			if (!options.mode2to1)
				printf("Mode2/2352");
			else
				printf("Mode1/2048");
			break;
		case MODE1_2048:
			printf("Mode1/2048");
			break;
		default:
			printf("Huh? What's going on?");
			exit(1);
	}
	printf(" :       ");

	if (options.sOutFilename[0] != '\0')
	{
		if (NULL == (fdOutFile = fopen(options.sOutFilename, "wb")))
			perror("bin2iso(fopen)");
		//printf("\nOpened File %s: %d\n", options.sOutFilename, fdOutFile);
	}
	else
	{
		fdOutFile = fdBinFile;
	}
	if (fdOutFile == NULL)
	{
		printf("    Unable to create %s\n", options.sOutFilename);
		exit(1);
	}

	if (0 != fseek(fdBinFile, offset, SEEK_SET))
	{
		perror("\nbin2iso(fseek)");
		exit(1);
	}

	#if (DEBUG == 0)
	if (mode == AUDIO)
	{
		if (1 != fwrite(&wavhead, sizeof(wavhead), 1, fdOutFile))
		{
			// write placeholder
			perror("\nbin2iso(fwrite)");
			fclose(fdOutFile);
			// remove(options.sOutFilename);
			exit(1);
		}
	}
	#endif

	memset(&buf[0], '\0', sizeof(buf));
	if (mode == MODE2_2336)
	{
		uint32_t M = 0, S = 2, F = 0;
		while (BufferedFRead(&buf[16], SIZEISO_MODE2_FORM2))
		{
			//setup headed area (probably not necessary though...
			//buf[0] = 0;
			memset(&buf[1], 0xFF, sizeof(buf[0]) * 10);
			//buf[11] = 0;
			buf[12] = M;
			buf[13] = S;
			buf[14] = F;
			buf[15] = MODE2;

			if ((++F&0xF) == 0xA)
				F += 6;

			if (F == 0x75)
			{
				S++;
				F = 0;
			}
			if ((S & 0xF) == 0xA)
				S += 6;

			if (S == 0x60)
			{
				M++;
				S = 0;
			}
			if ((M & 0xF) == 0xA)
				M += 6;
			//printf("\n%x:%x:%x", M, S, F);

			BufferedFWrite(buf, SIZERAW);
			uiLastIndex++;
			memset(&buf[0], '\0', sizeof(buf));
			if (startidx % PROG_INTERVAL == 0)
				printf("\b\b\b\b\b\b%06d", startidx);

			if (++startidx == endidx)
			{
				printf("\b\b\b\b\b\bComplete\n");
				break;
			}
		}
	}
	else if (mode == MODE1_2048)
	{
		while (BufferedFRead(buf, SIZEISO_MODE1))
		{
			BufferedFWrite(buf, SIZEISO_MODE1);
			uiLastIndex++;
			if (startidx % PROG_INTERVAL == 0)
				printf("\b\b\b\b\b\b%06d", startidx);

			if (++startidx == endidx)
			{
				printf("\b\b\b\b\b\bComplete\n");
				break;
			}
		}
	}
	else
	{
		while (BufferedFRead(buf, SIZERAW))
		{
			switch (mode)
			{
				case AUDIO:
					#if (DEBUG == 0)
					BufferedFWrite(buf, SIZERAW);
					#endif
					uiLastIndex++;
					blockswritten++;
					break;
				case MODE1_2352:
					// should put a crc check in here...
					#if CHECK
					if (buf[15] != MODE1)
					{
						printf("\nWarning: Mode Error in bin file!\n");
						printf("   %02x:%02x:%02x : mode %02x\n",
							buf[12], buf[13], buf[14], buf[15]);
						//exit(1);
					}

					uiCurrentIndex = Index(buf[12], buf[13], buf[14]) - OFFSET;

					if (uiCurrentIndex != uiLastIndex + 1)
					{
						printf("\nWarning: Frame Error in bin file!\n");
						printf("Last      %02d:%02d:%02d (%d)\n",
							((uiLastIndex + OFFSET) / 75) / 60,
							((uiLastIndex + OFFSET) / 75) % 60,
							(uiLastIndex + OFFSET) % 75,
							uiLastIndex);
						printf("Current   %02x:%02x:%02x (%d)\n",
							buf[12], buf[13], buf[14], uiCurrentIndex);
						printf("Expecting %02d:%02d:%02d (%d)\n",
							((uiLastIndex + OFFSET + 1) / 75) / 60,
							((uiLastIndex + OFFSET + 1) / 75) % 60,
							(uiLastIndex + OFFSET + 1) % 75,
							uiLastIndex + 1);
					}
					#endif
					#if (DEBUG == 0)
					BufferedFWrite(&buf[16], SIZEISO_MODE1);
					#endif
					#if CHECK
					uiLastIndex = uiCurrentIndex;
					#endif
					break;
				case MODE2_2352:
					#if CHECK
					if ((buf[15] & 0xF) != MODE2)
					{
						printf("\nWarning: Mode Error in bin file!\n");
						printf("   %02x:%02x:%02x : mode %02x\n",
							buf[12], buf[13], buf[14], buf[15]);
						//exit(1);
					}

					uiCurrentIndex = Index(buf[12], buf[13], buf[14]) - OFFSET;

					if (uiCurrentIndex != uiLastIndex + 1)
					{
						printf("\nWarning: Frame Error in bin file!\n");
						printf("Last      %02d:%02d:%02d (%d)\n",
							((uiLastIndex + OFFSET) / 75) / 60,
							((uiLastIndex + OFFSET) / 75) % 60,
							(uiLastIndex + OFFSET) % 75,
							uiLastIndex);
						printf("Current   %02x:%02x:%02x (%d)\n",
							buf[12], buf[13], buf[14], uiCurrentIndex);
						printf("Expecting %02d:%02d:%02d (%d)\n",
							((uiLastIndex + OFFSET + 1) / 75) / 60,
							((uiLastIndex + OFFSET + 1) / 75) % 60,
							(uiLastIndex + OFFSET + 1) % 75,
							uiLastIndex + 1);
					}
					#endif
					#if (DEBUG == 0)
					if (options.mode2to1)
						BufferedFWrite(&buf[16 + 8], SIZEISO_MODE1);
					else if (write)
						BufferedFWrite(&buf[0], SIZEISO_MODE2_RAW);
					#endif
					#if CHECK
					uiLastIndex = uiCurrentIndex;
					#endif
					break;
				default:
					printf("Unknown Mode\n");
					exit(1);
					break;
			}

			memset(&buf[0], '\0', sizeof(buf));
			if (startidx % PROG_INTERVAL == 0)
				printf("\b\b\b\b\b\b%06d", startidx);

			if (++startidx == endidx)
			{
				printf("\b\b\b\b\b\bComplete\n");
				break;
			}
		}
	}
	FlushBuffers();

	if (mode == AUDIO)
	{
		wavhead.blockSize = blockswritten * SIZERAW;
		wavhead.bytesToEnd = wavhead.blockSize + HEADBYTES;
		// rewind to the beginning
		if (0 != fseek(fdOutFile, 0, SEEK_SET))
		{
			perror("\nbin2iso(fseek)");
			exit(1);
		}

		#if (DEBUG == 0)
		fwrite(&wavhead, sizeof(wavhead), 1, fdOutFile);
		#endif
	}
	fclose(fdOutFile);
}

// Returns 0 when no data found, 1 when there is.
int8_t CheckGaps(FILE* fdBinFile, tTrack tracks[], uint32_t nTracks)
{
	if (nTracks == 2) // don't need to bother with single track images
		return 0;

	uint8_t buf[SIZERAW];
	int8_t _writegap = 0;

	printf("Checking gap data:\n");

	for (uint32_t i = 0; i < nTracks; i++)
	{
		if ((tracks[i].offset0 != tracks[i].offset1) && (tracks[i - 1].mode == AUDIO))
		{
			if (0 != fseek(fdBinFile, tracks[i].offset0, SEEK_SET))
			{
				perror("\nbin2iso(fseek)");
				exit(1);
			}

			uint32_t nonzerocount = 0;
			for (off_t j = tracks[i].idx0; j < tracks[i].idx1; j++)
			{
				if (0 == fread(buf, SIZERAW, 1, fdBinFile))
				{
					perror("bin2iso(fread)");
					exit(1);
				}
				for (size_t k = 0; k < SIZERAW; k += 2)
				{
					uint16_t value = buf[k + 1];
					value = ((value << 8) | buf[k]);
					if (value != 0)
					{
						nonzerocount++;
						// printf("%10d: %2x\n", nonzerocount, value);
					}
				}
			}
			if (nonzerocount != 0)
			{
				printf("Track%02u - %u values of non-zero gap data found\n", i - 1, nonzerocount);
				if ((nonzerocount > SIZERAW / 2 / 2) && _writegap == 0)
				{
					printf("   -->Threshold reached\n");
					_writegap = 1;
				}
			}
		}
	}
	return _writegap;
}

void CueFromBin()
{
	fdBinFile = fopen(options.sBinFilename, "rb");
	if (fdBinFile == NULL)
	{
		printf("Unable to open %s\n", options.sBinFilename);
		exit(1);
	}
	fdCueFile = fopen(options.sCueFilename, "w");
	if (fdCueFile == NULL)
	{
		printf("Unable to create %s\n", options.sCueFilename);
		exit(1);
	}

	int l = strlen(options.sBinFilename) - 4;
	if ((strcmp(&options.sBinFilename[l], ".wav") == 0) ||
		(strcmp(&options.sBinFilename[l], ".WAV") == 0))
	{
		printf(".wav binfile - Skipping wav header\n");
		fseek(fdBinFile, sizeof(tWavHead), SEEK_CUR);
	}

	printf(            "FILE %s BINARY\n", options.sBinFilename);
	fprintf(fdCueFile, "FILE %s BINARY\n", options.sBinFilename);

	char modetxt[13] = "AUDIO";
	char index0[9] = "00:00:00";
	char index1[9] = "00:00:00";
	uint8_t buf[SIZERAW+100];
	memset(buf, '\0', sizeof(buf));

	int32_t track = 1;
	uint32_t binIndex = 0;
	uint32_t trackIndex = 0;
	const uint32_t gapThreshold = 20; // look for 0.266 sec gap
	const uint16_t valueThreshold = 800; // look for samples < 700

	bool gapon = false;
	uint32_t count = 0;

	while (fread(buf, 1, SIZERAW, fdBinFile))
	{
		if (trackIndex == 0)
		{
			if ((buf[0] == 0x00) &&
				(buf[1] == 0xFF) &&
				(buf[2] == 0xFF) &&
				(buf[3] == 0xFF) &&
				(buf[4] == 0xFF) &&
				(buf[5] == 0xFF) &&
				(buf[6] == 0xFF) &&
				(buf[7] == 0xFF) &&
				(buf[8] == 0xFF) &&
				(buf[9] == 0xFF) &&
				(buf[10] == 0xFF) &&
				(buf[11] == 0x00)
			)
			{
				sprintf(modetxt, "MODE%d/2352", buf[15]);
			}
			else
			{
				sprintf(modetxt, "AUDIO");
			}
		}
		if (binIndex == 0)
		{
			printf(            "  TRACK %02d %s\n", track, modetxt);
			fprintf(fdCueFile, "  TRACK %02d %s\n", track, modetxt);
			printf(            "    INDEX 01 %s\n", index0);
			fprintf(fdCueFile, "    INDEX 01 %s\n", index0);
		}
		bool blank = true;
		for (uint32_t i = 0; i < SIZERAW; i += 2)
		{
			uint16_t value = buf[i + 1];
			value = ((value << 8) | buf[i]);
			//printf("%f %i\n", (1.0 / 75) * binIndex, value);
			if (abs(value) > valueThreshold)
			{
				blank = false;
				break;
			}
		}
		//if (i == SIZERAW) printf("%f ~blank~\n", (1.0 / 75) * binIndex);
		if (blank)
			count++;
		else if (gapon)
		{
			gapon = false;
			UnIndex(binIndex - count, index0);
			count = 0;
			UnIndex(binIndex, index1);
			printf(            "  TRACK %02d %s\n", track, modetxt);
			fprintf(fdCueFile, "  TRACK %02d %s\n", track, modetxt);
			printf(            "    INDEX 00 %s\n", index0);
			fprintf(fdCueFile, "    INDEX 00 %s\n", index0);
			printf(            "    INDEX 01 %s\n", index1);
			fprintf(fdCueFile, "    INDEX 01 %s\n", index1);
		}

		if ((count > gapThreshold) && !gapon)
		{
			gapon = true;
			track++;
			trackIndex = -1;
		}

		memset(buf, '\0', sizeof(buf));
		binIndex++;
		trackIndex++;
	}
	fclose(fdCueFile);
	fclose(fdBinFile);
}

void DoBinFile()
{
	fdCueFile = fopen(options.sCueFilename, "r");
	if (fdCueFile == NULL)
	{
		printf("Error: Unable to open \"%s\"\n", options.sCueFilename);
		exit(1);
	}

	// Get bin filename from cuefile... why? why not. Normally in form: FILE "x.bin" BINARY
	char sLine[256];
	if (!fgets(sLine, 256, fdCueFile))
	{
		printf("Error reading cuefile\n");
		exit(1);
	}
	if (strncmp(sLine, "FILE ", 5) != 0)
	{
		printf("Error: 'FILE' not found on first line of cuefile %s.\n", options.sCueFilename);
		exit(1);
	}

	char* readp = sLine + 5;
	char* writep = options.sBinFilename;
	int32_t quotecount = 0;

	// Skip leading spaces, just in case.
	while (*readp == ' ') readp++;

	while (true)
	{
		if (*readp >= 1 && *readp <= 31)
		{
			//printf("Error: 'FILE' in cuefile %s has control characters.\n", options.sCueFilename);
		}
		// Remove path info, just ignore anything to the left of every path separator.
		else if (*readp == '/' || *readp == '\\')
		{
			writep = options.sBinFilename;
		}
		else if (*readp == '"')
		{
			quotecount++;
			if (quotecount > 2)
			{
				printf("Error: Too many \" in 'FILE' in cuefile %s.\n", options.sCueFilename);
				exit(1);
			}
			if (quotecount == 2)
				break;
		}
		else
		{
			if (*readp == 0 || (*readp == ' ' && quotecount == 0))
				break;
			*(writep++) = *readp;
		}
		readp++;
	}
	if (quotecount == 1)
	{
		printf("Error: Unpaired \" in 'FILE' in cuefile %s.\n", options.sCueFilename);
		exit(1);
	}

	if (writep == options.sBinFilename)
	{
		printf("Error: Empty name for 'FILE' in cuefile %s.\n", options.sCueFilename);
		exit(1);
	}
	*writep = '\0';

	// Open the bin file.
	char* filemode = (options.doInPlace ? "rb+" : "rb");
	fdBinFile = fopen(options.sBinFilename, filemode);
	if (fdBinFile == NULL)
	{
		// If the bin file was not found at exact casing, look for a variant-case file.
		DIR* dir = opendir(".");
		if (dir != NULL)
		{
			int namelen = strlen(options.sBinFilename);
			char wantbin[namelen + 1];
			strcpylower(wantbin, options.sBinFilename);

			struct dirent* entry = NULL;
			while ((entry = readdir(dir)) != NULL)
			{
				int entrylen = strlen(entry->d_name);
				if (entrylen != namelen)
					continue;
				char newname[entrylen + 1];
				strcpylower(newname, entry->d_name);
				if (strcmp(wantbin, newname) == 0)
				{
					fdBinFile = fopen(entry->d_name, filemode);
					if (fdBinFile != NULL)
						break;
				}
			}
			closedir(dir);
		}
		if (fdBinFile == NULL)
		{
			printf("Error: Unable to open \"%s\"\n", options.sBinFilename);
			perror("bin2iso(fopen)");
			exit(1);
		}
	}

	// Get next line.
	if (!fgets(sLine, 256, fdCueFile))
	{
		printf("Error reading cuefile\n");
		exit(1);
	}

	int32_t i = strlen(options.outputDir);
	if (i > 0)
	{
		if ((options.outputDir[i - 1] != '/') && (options.outputDir[i - 1] != ':'))
		{
			strcat(options.outputDir, "/");
		}
	}

	uint32_t nTracks = 0;
	tTrack tracks[100];

	while (!feof(fdCueFile))
	{
		GetTrackInfo(sLine, &tracks[nTracks++]);
	}
	tracks[nTracks].idx0 = tracks[nTracks].idx1 = -1;

	switch (tracks[0].mode)
	{
		case MODE1_2048:
			tracks[0].offset0 = tracks[0].idx0 * SIZEISO_MODE1;
			break;
		case MODE2_2336:
			tracks[0].offset0 = tracks[0].idx0 * SIZEISO_MODE2_FORM2;
			break;
		default: // AUDIO, MODE1_2352, MODE2_2352:
			tracks[0].offset0 = tracks[0].idx0 * SIZERAW;
			break;
	}
	/* set offsets */

	if (0 != fseek(fdBinFile, 0, SEEK_END))
	{
		perror("bin2iso(fseek)");
		exit(1);
	}

	tracks[nTracks].offset0 = tracks[nTracks].offset1 = ftell(fdBinFile);

	for (i = 0; i < nTracks; i++)
	{
		switch (tracks[i].mode)
		{
			case MODE1_2048:
				tracks[i].offset1 =
					tracks[i].offset0 + (tracks[i].idx1 - tracks[i].idx0) * SIZEISO_MODE1;
				if (tracks[i + 1].idx0 != -1)
					tracks[i + 1].offset0 =
						tracks[i].offset1
						+ (tracks[i + 1].idx0 - tracks[i].idx1) * SIZEISO_MODE1;
				else
				{
					tracks[i + 1].idx0 = tracks[i + 1].idx1 =
						(tracks[i + 1].offset0 - tracks[i].offset1) / SIZEISO_MODE1
						+ tracks[i].idx1;
					if (((tracks[i + 1].offset0 - tracks[i].offset1) % SIZEISO_MODE1) != 0)
						printf("Warning: Bin file has invalid byte count for cuefile.\n");
				}
				break;
			case MODE2_2336:
				tracks[i].offset1 =
					tracks[i].offset0
					+ (tracks[i].idx1 - tracks[i].idx0) * SIZEISO_MODE2_FORM2;
				if (tracks[i + 1].idx0 != -1)
					tracks[i + 1].offset0 =
						tracks[i].offset1
						+ (tracks[i + 1].idx0 - tracks[i].idx1) * SIZEISO_MODE2_FORM2;
				else
				{
					tracks[i + 1].idx0 = tracks[i + 1].idx1 =
						(tracks[i + 1].offset0 - tracks[i].offset1) / SIZEISO_MODE2_FORM2
						+ tracks[i].idx1;
					if (((tracks[i + 1].offset0 - tracks[i].offset1) % SIZEISO_MODE2_FORM2) != 0)
						printf("Warning: Bin file has invalid byte count for cuefile.\n");
				}
				break;
			default: // AUDIO, MODE1_2352, MODE2_2352:
				tracks[i].offset1 =
					tracks[i].offset0 + (tracks[i].idx1 - tracks[i].idx0) * SIZERAW;
				if (tracks[i + 1].idx0 != -1)
					tracks[i + 1].offset0 =
						tracks[i].offset1 + (tracks[i + 1].idx0 - tracks[i].idx1) * SIZERAW;
				else
				{
					tracks[i + 1].idx0 = tracks[i + 1].idx1 =
						(tracks[i + 1].offset0 - tracks[i].offset1) / SIZERAW
						+ tracks[i].idx1;
					if (((tracks[i + 1].offset0 - tracks[i].offset1) % SIZERAW) != 0)
						printf("Warning: Bin file has invalid byte count for cuefile.\n");
				}
				break;
		}
	}

	// if not allowing overburn, then create a new track to hold extra data...
	if (options.no_overburn)
	{
		i = nTracks;
		if (tracks[i].idx0 > CD74_MAX_SECTORS)
		{
			tracks[i + 1] = tracks[nTracks];
			strcpy(tracks[i].name, "obdatatemp.bin");
			tracks[i].idx0 = CD74_MAX_SECTORS;
			tracks[i].idx1 = CD74_MAX_SECTORS;
			switch (tracks[i - 1].mode)
			{
				case MODE1_2048:
					tracks[i].offset0 =
						tracks[i - 1].offset1
						+ (tracks[i].idx0 - tracks[i - 1].idx1) * SIZEISO_MODE1;
					break;
				case MODE2_2336:
					tracks[i].offset0 =
						tracks[i - 1].offset1
						+ (tracks[i].idx0 - tracks[i - 1].idx1) * SIZEISO_MODE2_FORM2;
					break;
				default: // AUDIO, MODE1_2352, MODE2_2352:
					tracks[i].offset0 =
						tracks[i - 1].offset1
						+ (tracks[i].idx0 - tracks[i - 1].idx1) * SIZERAW;
					break;
			}
			tracks[i].offset1 = tracks[i].offset0;
			tracks[i].mode = tracks[i - 1].mode;
			nTracks++;
		}
	}

	/* set sizes */
	for (i = 0; i < nTracks; i++)
	{
		switch (tracks[i].mode)
		{
			case MODE1_2352:
				tracks[i].size =
					((tracks[i + 1].offset1 - tracks[i].offset1) / SIZERAW) * SIZEISO_MODE1;
				break;
			case MODE2_2336:
				tracks[i].size =
					((tracks[i + 1].offset1 - tracks[i].offset1) / SIZEISO_MODE2_FORM2) * SIZERAW;
				break;
			default: // MODE1_2048, MODE2_2352, AUDIO
				tracks[i].size = tracks[i + 1].offset1 - tracks[i].offset1;
				break;
		}
	}

	if (options.writegap == -1)
		options.writegap = CheckGaps(fdBinFile, tracks, nTracks);

	if (options.writegap == 1)
		printf("Note: Appending pregap data to end of audio tracks\n");
	else
		printf("Note: Discarding pregap data\n");

	printf("\n");
	for (i = 0; i <= nTracks - 1; i++)
	{
		printf("%s (%lu Mb) - sectors %06ld:%06ld (offset %09ld:%09ld)\n",
			tracks[i].name,
			tracks[i].size / (1024 * 1024),
			tracks[i].idx1,
			(((options.writegap == 0) || (tracks[i].mode != AUDIO))
				? tracks[i + 1].idx0 : tracks[i + 1].idx1) - 1,
			tracks[i].offset1,
			(((options.writegap == 0) || (tracks[i].mode != AUDIO))
				? tracks[i + 1].offset0 : tracks[i + 1].offset1) - 1
		);
	}
	printf("\n");

	if (
		((!options.mode2to1 && (tracks[0].mode == MODE2_2352)) || (tracks[0].mode == MODE1_2048))
		&& (nTracks == 1))
	{
		if (tracks[0].mode == MODE2_2352)
			printf("Mode2/2352");

		if (tracks[0].mode == MODE1_2048)
			printf("Mode1/2048");

		printf(" single track bin file indicated by cue file\n");
		fclose(fdBinFile);
		if (0 != rename(options.sBinFilename, tracks[0].name))
		{
			perror("\nbin2iso(rename)");
			exit(1);
		}
		printf("%s renamed to %s\n", options.sBinFilename, tracks[0].name);
		fclose(fdCueFile);
		exit(0);
	}

	for (i = nTracks - 1; i >= 0; i--)
	{
		tTrack trackA = tracks[i];
		tTrack trackB = tracks[i + 1];
		// audio can't be done in the bin file due to header.
		// 2336 can't either because it's expanded to 2352
		if (options.doInPlace && (i == 0) && (trackA.mode != AUDIO) && (trackA.mode != MODE2_2336))
		{
			options.sOutFilename[0] = '\0';
		}
		else
		{
			strcpy(options.sOutFilename, options.outputDir);
			strcat(options.sOutFilename, trackA.name);
		}

		if ((!options.doOneTrack || trackA.num == options.oneTrackNum))
		{
			if (!((i == 0) && ((trackA.mode == MODE2_2352)
				|| (trackA.mode == MODE1_2048)) && options.doInPlace))
			{
				if (options.writegap == 0 || (trackA.mode != AUDIO))
				{ // when not Audio, don't append.
					DoTrack(
						trackA.mode, trackA.idx0, trackA.idx1, trackB.idx0, trackA.offset1);
				}
				else
				{
					/* if (trackA.idx0 == 0) // handles first track with pregap.
						DoTrack(trackA.mode, 0, trackA.idx1, trackB.idx1, trackA.offset1);
					else
					*/
					DoTrack(
						trackA.mode, trackA.idx1, trackA.idx1, trackB.idx1, trackA.offset1);
				}
			}
		}
		/*else
		{
			fclose(fdBinFile); // just close bin file. Already MODE1_2048 or MODE2_2352
		}*/
		if (!options.doOneTrack && options.doInPlace)
		{
			if ((i != 0) || (trackA.mode == AUDIO) || (trackA.mode == MODE2_2336))
			{
				printf("Truncating bin file to %ld bytes\n", trackA.offset1);
				if (-1 == ftruncate(fileno(fdBinFile), trackA.offset1))
				{
					perror("\nbin2iso(_chsize)");
					exit(1);
				}
			}
			else
			{
				printf("Renaming %s to %s\n", options.sBinFilename, trackA.name);
				fclose(fdBinFile);
				if (0 != rename(options.sBinFilename, trackA.name))
				{
					perror("\nbin2iso(rename)");
					exit(1);
				}

				// fix writepos for case when simply truncating...
				if ((trackA.mode == MODE2_2352) || (trackA.mode == MODE1_2048))
					writePos = trackB.offset0;

				printf("Truncating to %ld bytes\n", writePos);

				fdBinFile = fopen(trackA.name, "rb+"); // gets closed in DoTrack...
				if (fdBinFile == NULL)
				{
					perror("bin2iso(fopen)");
					exit(1);
				}

				if (-1 == ftruncate(fileno(fdBinFile), writePos))
				{
					perror("\nbin2iso(_chsize)");
					exit(1);
				}
			}
		}
	}
	fclose(fdCueFile);
	fclose(fdBinFile);
}

/* /\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/ */

void PrintHelp()
{
	printf("\nbin2iso "VERSIONSTR" - Converts RAW format (.bin) files to ISO/WAV format");
	printf("\nOriginal by Bob Doiron, this version by Kirinn Bunnylin");
	printf("\nhttps://gitlab.com/bunnylin/bin2iso\n\n");
	printf("Usage: bin2iso <cuefile> [<output dir>] [-naib2] [-t X]\n");
	printf("or   : bin2iso <cuefile> -c <binfile>\n");
	printf("\nWhere:\n");
	printf("  <cuefile>     The .cue file that belongs to the .bin file being converted.\n");
	printf("  <output dir>  The output directory (defaults to current dir).\n");
	printf("  -n --nogaps   Indicates that audio data found in the track 'gaps' should\n");
	printf("                not be appended to the wave files.\n");
	printf("  -a --gaps     Looks for non-zero data in the 'gaps'; if found, then gaps\n");
	printf("                are appended to wave files. Looks for more than 1/2 of\n");
	printf("                a sector of non-zero values (%d values).\n", SIZERAW / 2 / 2);
	printf("  -t --track=X  Extracts only the X'th track.\n");
	printf("  -i --inplace  Performs the conversion 'in place'; ie. truncates the binfile\n");
	printf("                after each track is created to minimize space usage.\n");
	printf("                (can't be used with -t)\n");
	printf("  -b --nob      Do not use overburn data past %d sectors.\n", CD74_MAX_SECTORS);
	printf("                This of course presumes that the data is not useful.\n");
	printf("  -c --cuefrom=<binfile>\n");
	printf("                Attempts to create a <cuefile> from an existing <binfile>.\n");
	printf("  -2 --mode2to1 Attempts to convert mode2 ISO data to mode1.\n");
}

bool ParseArgs(int argc, char* argv[])
{
	const char* const short_opts = "nat:ibc:2vh?";
	static struct option long_opts[] = {
		{ "nogaps", no_argument, NULL, 'n' },
		{ "gaps", no_argument, NULL, 'a' },
		{ "track", required_argument, NULL, 't' },
		{ "inplace", no_argument, NULL, 'i' },
		{ "nob", no_argument, NULL, 'b' },
		{ "cuefrom", required_argument, NULL, 'c' },
		{ "mode2to1", no_argument, NULL, '2' },
		{ "version", no_argument, NULL, 'v' },
		{ "help", no_argument, NULL, 'h' },
		{ NULL, no_argument, NULL, 0 }
	};
	extern int optind;

	//options.writegap = -1; // auto detect pregap data action by default.
	options.writegap = 1; // keep pregap data by default.
	options.sOutFilename[0] = '\0';
	strcpy(options.outputDir, "./"); // default path

	while (true)
	{
		int opt = getopt_long(argc, argv, short_opts, long_opts, NULL);
		if (opt == -1)
			break;

		if (optarg)
			if (optarg[0] == '=' || optarg[0] == ':')
				optarg++;

		switch (opt)
		{
			case 'n':
				options.writegap = 0;
				break;

			case 'a':
				options.writegap = -1;
				printf("Note: Auto-detecting pregap data\n");
				break;

			case 't':
			{
				if (options.doInPlace)
				{
					printf("Can't use -t and -i together.\n");
					return false;
				}
				char* strend;
				options.oneTrackNum = strtol(optarg, &strend, 10);
				options.doOneTrack = true;
				break;
			}

			case 'i':
				if (options.doOneTrack)
				{
					printf("Can't use -t and -i together.\n");
					return false;
				}
				printf("Bin file will be truncated after each track created\n");
				options.doInPlace = true;
				break;

			case 'b':
				options.no_overburn = true;
				break;

			case 'c':
				options.createCue = true;
				strcpy(options.sBinFilename, optarg);
				break;

			case '2':
				options.mode2to1 = true;
				printf("Note: Converting Mode2 ISO to Mode1\n");
				break;

			case 'v':
				printf(VERSIONSTR"\n");
				exit(0);
				break;

			case '?':
			case 'h':
			default:
				return false;
		}
	}

	if (optind == argc)
	{
		printf("<cuefile> must be specified.\n");
		return false;
	}

	strcpy(options.sCueFilename, argv[optind++]);
	if (optind < argc)
	{
		strcpy(options.outputDir, argv[optind++]);
	}
	if (optind < argc)
	{
		printf("Unexpected argument: %s\n", argv[optind]);
		return false;
	}

	return true;
}

int main(int argc, char* argv[])
{
	if (!ParseArgs(argc, argv))
	{
		PrintHelp();
		return 1;
	}

	if (options.createCue)
		CueFromBin();
	else
		DoBinFile();

	return 0;
}
